﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Xps.Packaging;
namespace CSharpEducation
{
    /// <summary>
    /// Page1.xaml etkileşim mantığı
    /// </summary>
    public partial class Page1 : Page
    {
        public Page1()
        {
            InitializeComponent();
        }

        

        private void ÇIKIŞ_Click(object sender, RoutedEventArgs e)
        {
            Page2 page = new Page2();
            this.NavigationService.Navigate(page);
        }

        private void hakkında_Click(object sender, RoutedEventArgs e)
        {
            
             About.about page = new About.about();
            this.NavigationService.Navigate(page);
           


        }

        private void Konular_Click(object sender, RoutedEventArgs e)
        {
            Ckonular page = new Ckonular();
            this.NavigationService.Navigate(page);
        }

        private void Videoss_Click(object sender, RoutedEventArgs e)
        {
            Videos.VideosEnter page = new Videos.VideosEnter();
            this.NavigationService.Navigate(page);
        }
    }
}
