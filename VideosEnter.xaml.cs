﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace CSharpEducation.Videos
{
    /// <summary>
    /// VideosEnter.xaml etkileşim mantığı
    /// </summary>
    public partial class VideosEnter : Page
    {
        public VideosEnter()
        {
            InitializeComponent();

            //mePlayer.Source = new Uri("C:/Users/hsyns/Desktop/cd.png");

            addCombo();
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();


        }




        void timer_Tick(object sender, EventArgs e)
        {
            if (mePlayer.Source != null)
            {
                if (mePlayer.NaturalDuration.HasTimeSpan)
                    lblStatus.Content = String.Format("{0} / {1}", mePlayer.Position.ToString(@"mm\:ss"), mePlayer.NaturalDuration.TimeSpan.ToString(@"mm\:ss"));

                sliderss.Value = mePlayer.Position.TotalSeconds;
            }
            else
                lblStatus.Content = "No file selected...";
        }

        private void Helloo_Click(object sender, RoutedEventArgs e)
        {

        }

        
        

        private void mePlayer_MediaOpened(object sender, RoutedEventArgs e)
        {
            if (mePlayer.NaturalDuration.HasTimeSpan)
            {
                TimeSpan ts = TimeSpan.FromMilliseconds(mePlayer.NaturalDuration.TimeSpan.TotalMilliseconds);
                sliderss.Maximum = ts.TotalSeconds;

            }
        }

        private void sliderss_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            TimeSpan ts = TimeSpan.FromSeconds(e.NewValue);
            mePlayer.Position = ts;
        }

        private void btnPause_Click(object sender, RoutedEventArgs e)
        {
            mePlayer.Pause();
        }

        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            mePlayer.Play();
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            mePlayer.Stop();
        }

        private void voice_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }

        private void addCombo()
        {

            First.Items.Add("Installing Visual Studio Community");
            First.Items.Add("Hello World");
            First.Items.Add("Changing The Console Colors");

            Data.Items.Add("Intro To Datatypes And What To Consider");
            Data.Items.Add("Datatypes Int, Float and Double");
            Data.Items.Add("Datatype String And Some Of Its Methods");
            Data.Items.Add("Naming Conventions and Coding Standards");
            Data.Items.Add("Implicit and Explicit Conversion");
            Data.Items.Add("Parsing a String To An Integer");
            Data.Items.Add("Solution For The Challenge Datatypes And Variables");
            Data.Items.Add("Constants");

            Method.Items.Add("Intro To Functions  Methods");
            Method.Items.Add("Void Methods");
            Method.Items.Add("Methods With Return Value And Parameters");
            Method.Items.Add("Solution For The Challenge Methods");
            Method.Items.Add("Operators");

            Decision.Items.Add("Introduction To Decision Making In C#");
            Decision.Items.Add("IF And Else If + Try Parse");
            Decision.Items.Add("Nested If Statements");
            Decision.Items.Add("Solution For The Challenge If Statements");
            Decision.Items.Add("Switch Statement");
            Decision.Items.Add("Solution For The Challenge If Statements 2");
            Decision.Items.Add("Enhanced If Statements -  Ternary Operator");

            Loop.Items.Add("Basics of Loops");
            Loop.Items.Add("For Loops");
            Loop.Items.Add("Do While Loops");
            Loop.Items.Add("While Loops");
            Loop.Items.Add("break and continue");
            Loop.Items.Add("Solution For The Challenge Loops");

            Objectt.Items.Add("Introduction To Classes And Objects");
            Objectt.Items.Add("Our First Own Class");
            Objectt.Items.Add("Using Constructors");
            Objectt.Items.Add("Using Multiple Constructors");
            Objectt.Items.Add("Properties");
            Objectt.Items.Add("Challenge - Properties");
            Objectt.Items.Add("Members And FinalizersDestructors");

            Arrayy.Items.Add("Basics of Arrays");
            Arrayy.Items.Add("Declaring and Initializing Arrays and the Length Property");
            Arrayy.Items.Add("Foreach Loops");
            Arrayy.Items.Add("Multi Dimensional Arrays");
            Arrayy.Items.Add("Challenge - Tic Tac Toe");
            Arrayy.Items.Add("Jagged Arrays");
            Arrayy.Items.Add("Challenge - Jagged Arrays");
            Arrayy.Items.Add("Using Arrays As Parameters");
            Arrayy.Items.Add("ArrayLists");

            Inharitance.Items.Add("Introduction To Inheritance");
            Inharitance.Items.Add("Inheritance Demo");
            Inharitance.Items.Add("Inheritance Challenge - Videopost and Timer with Callback");
            Inharitance.Items.Add("Inheritance Challenge 2 - Employees, Bosses and Trainees Solution");

            Polyy.Items.Add("Polymorphic Parameters");
            Polyy.Items.Add("Sealed Key Word");
            Polyy.Items.Add("Has A - Relationships");
            Polyy.Items.Add("Read from a Textfile");
            Polyy.Items.Add("Write into a Text File");

            Advance.Items.Add("Lambda Expressions");
            Advance.Items.Add("Arraylists vs Lists vs Arrays");
            Advance.Items.Add("Structs");
            Advance.Items.Add("Enums");
            Advance.Items.Add("Math Class");
            Advance.Items.Add("Random Class");
            Advance.Items.Add("Regular Expressions");
            Advance.Items.Add("DateTime");
            Advance.Items.Add("Nullables");





        }

        private void First_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (First.SelectedIndex == 0)
            { mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/1. Your First C# Programm And Overview Of Visual Studio/4. Installing Visual Studio Community.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Installing Visual Studio Community";
             }
            if (First.SelectedIndex == 1)
            { mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/1. Your First C# Programm And Overview Of Visual Studio/5. Hello World - First Program.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Hello World";
            }
            if (First.SelectedIndex == 2)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/1. Your First C# Programm And Overview Of Visual Studio/7. Changing The Consoles Colors.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Changing The Console Colors";
            }


        }

        private void Data_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Data.SelectedIndex == 0)
            { mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/2. DataTypes And Variables/2. Intro To Datatypes And What To Consider.mp4", UriKind.RelativeOrAbsolute);

                lbName.Content = "Intro To Datatypes And What To Consider";
            }
            if (Data.SelectedIndex == 1)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/2. DataTypes And Variables/3. Datatypes Int, Float and Double.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Datatypes Int, Float and Double";

            }
            if (Data.SelectedIndex == 2)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/2. DataTypes And Variables/4. Datatype String And Some Of Its Methods.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Datatype String And Some Of Its Methods";

            }
            if (Data.SelectedIndex == 3)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/2. DataTypes And Variables/6. Naming Conventions and Coding Standards.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Naming Conventions and Coding Standards";

            }
            if (Data.SelectedIndex == 4)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/2. DataTypes And Variables/7. Implicit and Explicit Conversion.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Implicit and Explicit Conversion";

            }
            if (Data.SelectedIndex == 5)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/2. DataTypes And Variables/8. Parsing a String To An Integer.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Parsing a String To An Integer";

            }
            if (Data.SelectedIndex == 6)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/2. DataTypes And Variables/10. Solution For The Challenge Datatypes And Variables.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Solution For The Challenge Datatypes And Variables";

            }
            if (Data.SelectedIndex == 7)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/2. DataTypes And Variables/11. Constants.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Constants";

            }






        }


        private void Method_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Method.SelectedIndex == 0)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/3. Functions  Methods And How To Save Time/2. Intro To Functions  Methods.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Intro To Functions  Methods";
            }
            if (Method.SelectedIndex == 1)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/3. Functions  Methods And How To Save Time/3. Void Methods.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Void Methods";
            }
            if (Method.SelectedIndex == 2)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/3. Functions  Methods And How To Save Time/4. Methods With Return Value And Parameters.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Methods With Return Value And Parameters";
            }
            if (Method.SelectedIndex == 3)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/3. Functions  Methods And How To Save Time/7. Solution For The Challenge Methods.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Solution For The Challenge Methods";

            }
            if (Method.SelectedIndex == 4)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/3. Functions  Methods And How To Save Time/10. Operators.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Operators";

            }
        }

        private void Decision_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Decision.SelectedIndex == 0)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/4. Making Decisions/2. Introduction To Decision Making In C#.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Introduction To Decision Making In C#";
            }
            if (Decision.SelectedIndex == 1)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/4. Making Decisions/3. IF And Else If + Try Parse.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "IF And Else If + Try Parse";
            }
            if (Decision.SelectedIndex == 2)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/4. Making Decisions/4. Nested If Statements.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Nested If Statements";
            }
            if (Decision.SelectedIndex == 3)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/4. Making Decisions/6. Solution For The Challenge If Statements.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Solution For The Challenge If Statements";
            }
            if (Decision.SelectedIndex == 4)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/4. Making Decisions/7. Switch Statement.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Switch Statement";

            }
            if (Decision.SelectedIndex == 5)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/4. Making Decisions/9. Solution For The Challenge If Statements 2.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Solution For The Challenge If Statements 2";
            }
            if (Decision.SelectedIndex == 6)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/4. Making Decisions/10. Enhanced If Statements -  Ternary Operator.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Enhanced If Statements -  Ternary Operator";
            }


        }

        private void Loop_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Loop.SelectedIndex == 0)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/5. Loops/2. Basics of Loops.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Basics of Loops";
            }
            if (Loop.SelectedIndex == 1)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/5. Loops/3. For Loops.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "For Loops";
            }
            if (Loop.SelectedIndex == 2)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/5. Loops/4. Do While Loops.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Do While Loops";
            }
            if (Loop.SelectedIndex == 3)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/5. Loops/5. While Loops.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "While Loops";
            }
            if (Loop.SelectedIndex ==4)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/5. Loops/6. break and continue.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "break and continue";
            }
            if (Loop.SelectedIndex == 5)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/5. Loops/8. Solution For The Challenge Loops.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Solution For The Challenge Loops";
            }
        }

        private void Objectt_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Objectt.SelectedIndex == 0)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/6. Object Oriented Programming (OOP)/2. Introduction To Classes And Objects.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Introduction To Classes And Objects";
            }
            if (Objectt.SelectedIndex == 1)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/6. Object Oriented Programming (OOP)/3. Our First Own Class.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Our First Own Class";
            }
            if (Objectt.SelectedIndex == 2)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/6. Object Oriented Programming (OOP)/4. Using Constructors.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Using Constructors";
            }
            if (Objectt.SelectedIndex == 3)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/6. Object Oriented Programming (OOP)/5. Using Multiple Constructors.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Using Multiple Constructors";
            }
            if (Objectt.SelectedIndex == 4)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/6. Object Oriented Programming (OOP)/8. Properties.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Properties";
            }
            if (Objectt.SelectedIndex == 5)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/6. Object Oriented Programming (OOP)/9. Challenge - Properties.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Challenge - Properties";
            }
            if (Objectt.SelectedIndex == 6)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/6. Object Oriented Programming (OOP)/10. Members And FinalizersDestructors.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Members And FinalizersDestructors";
            }
        }

        private void Arrayy_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Objectt.SelectedIndex == 0)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/7. Arrays/2. Basics of Arrays.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Basics of Arrays";
            }
            if (Objectt.SelectedIndex == 1)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/7. Arrays/3. Declaring and Initializing Arrays and the Length Property.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Declaring and Initializing Arrays and the Length Property";
            }
            if (Objectt.SelectedIndex == 2)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/7. Arrays/4. Foreach Loops.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Foreach Loops";
            }
            if (Objectt.SelectedIndex == 3)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/7. Arrays/5. Multi Dimensional Arrays.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Multi Dimensional Arrays";
            }
            if (Objectt.SelectedIndex == 4)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/7. Arrays/7. Challenge - Tic Tac Toe.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Challenge - Tic Tac Toe";
            }
            if (Objectt.SelectedIndex == 5)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/7. Arrays/8. Jagged Arrays.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Jagged Arrays";
            }
            if (Objectt.SelectedIndex == 6)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/7. Arrays/9. Challenge - Jagged Arrays.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Challenge - Jagged Arrays";

            }
            if (Objectt.SelectedIndex == 7)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/7. Arrays/10. Using Arrays As Parameters.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Using Arrays As Parameters";
            }
            if (Objectt.SelectedIndex == 8)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/7. Arrays/11. ArrayLists.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "ArrayLists";
            }
        }

        private void Inharitance_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Inharitance.SelectedIndex == 0)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/8. Inheritance And More About OOP/2. Introduction To Inheritance.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Introduction To Inheritance";
            }
            if (Inharitance.SelectedIndex == 1)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/8. Inheritance And More About OOP/3. Inheritance Demo.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Inheritance Demo";
            }
            if (Inharitance.SelectedIndex == 2)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/8. Inheritance And More About OOP/4. Inheritance Challenge - Videopost and Timer with Callback.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Inheritance Challenge - Videopost and Timer with Callback";
            }
            if (Inharitance.SelectedIndex == 3)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/8. Inheritance And More About OOP/6. Inheritance Challenge 2 - Employees, Bosses and Trainees Solution.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Inheritance Challenge 2 - Employees, Bosses and Trainees Solution";
            }
           

        }

        private void Polyy_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Polyy.SelectedIndex == 0)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/9. Polymorphism And Even More On OOP + Text Files/2. Polymorphic Parameters.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Polymorphic Parameters";
            }
            if (Polyy.SelectedIndex == 1)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/9. Polymorphism And Even More On OOP + Text Files/3. Sealed Key Word.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Sealed Key Word";
            }
            if (Polyy.SelectedIndex == 2)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/9. Polymorphism And Even More On OOP + Text Files/4. Has A - Relationships.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Has A - Relationships";
            }
            if (Polyy.SelectedIndex == 3)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/9. Polymorphism And Even More On OOP + Text Files/5. Read from a Textfile.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Read from a Textfile";
            }
            if (Polyy.SelectedIndex == 4)
            {
                mePlayer.Source = new Uri("D:/Video_Document/[FreeTutorials.Us] Udemy - Complete C# Masterclass/9. Polymorphism And Even More On OOP + Text Files/6. Write into a Text File.mp4", UriKind.RelativeOrAbsolute);
                lbName.Content = "Write into a Text File";
            }
        }

        private void Advance_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
